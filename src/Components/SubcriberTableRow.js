import React from "react";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import axios from "axios";

const SubcriberTableRow = (props) => {
const { _id, name, email, rollno } = props.obj;

const deleteSubcriber = () => {
	axios
	.delete(
"http://localhost:3000/subcribers/delete-subcriber/" + _id)
	.then((res) => {
		if (res.status === 200) {
		alert("Subcriber successfully deleted");
		window.location.reload();
		} else Promise.reject();
	})
	.catch((err) => alert("Something went wrong"));
};

return (
	<tr>
	<td>{name}</td>
	<td>{email}</td>
	<td>{rollno}</td>
	<td>
		<Link className="edit-link"
		to={"/edit-subcriber/" + _id}>
		Edit
		</Link>
		<Button onClick={deleteSubcriber}
		size="sm" variant="danger">
		Delete
		</Button>
	</td>
	</tr>
);
};

export default SubcriberTableRow;
