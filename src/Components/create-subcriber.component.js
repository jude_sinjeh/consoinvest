// Create Subcriber Component for add new subcriber

// Import Modules
import React, { useState, useEffect } from "react";
import axios from 'axios';
import SubcriberForm from "./SubcriberForm";

// Create Subcriber Component
const CreateSubcriber = () => {
const [formValues, setFormValues] =
	useState({ name: '', email: '', rollno: '' })
// onSubmit handler
const onSubmit = subcriberObject => {
	axios.post(
'http://localhost:3000/subcriber/create-subcriber',
	subcriberObject)
	.then(res => {
		if (res.status === 200)
		alert('subcriber successfully created')
		else
		Promise.reject()
	})
	.catch(err => alert('Something went wrong'))
}
	
// Return subcriber form
return(
	<SubcriberForm initialValues={formValues}
	onSubmit={onSubmit}
	enableReinitialize>
	Create subcriber
	</SubcriberForm>
)
}

// Export CreateSubcriber Component
export default CreateSubcriber
