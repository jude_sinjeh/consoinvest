import React from "react";
import * as Yup from "yup";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { FormGroup, FormControl, Button } from "react-bootstrap";

const SubcriberForm = (props) => {
const validationSchema = Yup.object().shape({
	name: Yup.string().required("Rquired"),
	email: Yup.string()
	.email("You have enter an invalid email address")
	.required("Rquired"),
	rollno: Yup.number()
	.positive("Invalid roll number")
	.integer("Invalid roll number")
	.required("Rquired"),
});
console.log(props);
return (
	<div className="form-wrapper">
	<Formik {...props} validationSchema={validationSchema}>
		<Form>
		<FormGroup>
            <label htmlFor="name">Nom</label>
			<Field name="name" type="text"
				className="form-control" />
			<ErrorMessage
			name="name"
			className="d-block invalid-feedback"
			component="span"
			/>
		</FormGroup>
        <FormGroup>
            <label htmlFor="name">Prenom</label>
			<Field name="lastname" type="text"
				className="form-control" />
			<ErrorMessage
			name="lastname"
			className="d-block invalid-feedback"
			component="span"
			/>
		</FormGroup>
		<FormGroup>
            <label htmlFor="email">email</label>
			<Field name="email" type="text"
				className="form-control" />
			<ErrorMessage
			name="email"
			className="d-block invalid-feedback"
			component="span"
			/>
		</FormGroup>
		<FormGroup>
            <label htmlFor="number">Telephone</label>
			<Field name="rollno" type="number"
				className="form-control" />
			<ErrorMessage
			name="rollno"
			className="d-block invalid-feedback"
			component="span"
			/>
		</FormGroup>
        <FormGroup>
            <label htmlFor="name">Post Occupé/ Activités</label>
			<Field name="role" type="text"
				className="form-control" />
			<ErrorMessage
			name="role"
			className="d-block invalid-feedback"
			component="span"
			/>
		</FormGroup>
        <FormGroup>
            <label htmlFor="name">Entreprise/ Employeur</label>
			<Field name="company" type="text"
				className="form-control" />
			<ErrorMessage
			name="company"
			className="d-block invalid-feedback"
			component="span"
			/>
		</FormGroup>
       
        <FormGroup>
            <label htmlFor="name"> Quartier de résidence</label>
			<Field name="address" type="text"
				className="form-control" />
			<ErrorMessage
			name="address"
			className="d-block invalid-feedback"
			component="span"
			/>
		</FormGroup>
        <div>

        </div>
		<Button variant="primary" size="lg"
			block="block" type="submit">
			{props.children}
		</Button>
		</Form>
	</Formik>
	</div>
);
};

export default SubcriberForm;
