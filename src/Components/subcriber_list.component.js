import React, { useState, useEffect } from "react";
import axios from "axios";
import { Table } from "react-bootstrap";
import SubcriberTableRow from "./SubcriberTableRow";

const SubcriberList = () => {
const [subcribers, setSubcribers] = useState([]);

useEffect(() => {
	axios
	.get("http://localhost:3000/subcribers/")
	.then(({ data }) => {
		setSubcribers(data);
	})
	.catch((error) => {
		console.log(error);
	});
}, []);

const DataTable = () => {
	return subcribers.map((res, i) => {
	return <SubcriberTableRow obj={res} key={i} />;
	});
};

return (
	<div className="table-wrapper">
	<Table striped bordered hover>
		<thead>
		<tr>
			<th>Name</th>
			<th>Email</th>
			<th>Roll No</th>
			<th>Action</th>
		</tr>
		</thead>
		<tbody>{DataTable()}</tbody>
	</Table>
	</div>
);
};

export default SubcriberList;
