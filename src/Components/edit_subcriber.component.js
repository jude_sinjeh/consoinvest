// EditSubcriber Component for update Subcriber data

// Import Modules
import React, { useState, useEffect } from "react";
import axios from "axios";
import SubcriberForm from "./SubcriberForm";

// EditSubcriber Component
const EditSubcriber = (props) => {
const [formValues, setFormValues] = useState({
	name: "",
	email: "",
	rollno: "",
});
	
//onSubmit handler
const onSubmit = (SubcriberObject) => {
	axios
	.put(
		"http://localhost:3000/subcribers/update-subcriber/" +
		props.match.params.id,
		SubcriberObject
	)
	.then((res) => {
		if (res.status === 200) {
		alert("Subcriber successfully updated");
		props.history.push("/Subcriber-list");
		} else Promise.reject();
	})
	.catch((err) => alert("Something went wrong"));
};

// Load data from server and reinitialize Subcriber form
useEffect(() => {
	axios
	.get(
		"http://localhost:3000/subcribers/update-subcriber/"
		+ props.match.params.id
	)
	.then((res) => {
		const { name, email, rollno } = res.data;
		setFormValues({ name, email, rollno });
	})
	.catch((err) => console.log(err));
}, []);

// Return Subcriber form
return (
	<SubcriberForm
	initialValues={formValues}
	onSubmit={onSubmit}
	enableReinitialize
	>
	Update Subcriber
	</SubcriberForm>
);
};

// Export EditSubcriber Component
export default EditSubcriber;
