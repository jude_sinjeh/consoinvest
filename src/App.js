// Import React
import React from "react";

import logo from './logo.jpg';

// Import Bootstrap
import { Nav, Navbar, Container, Row, Col }
		from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.css";

// Import Custom CSS
import "./App.css";

// Import from react-router-dom
import { BrowserRouter as Router, Routes,
	Route, Link } from "react-router-dom";

// Import other React Component
import CreateSubcriber from
	"./Components/create-subcriber.component";
import EditSubcriber from
	"./Components/edit_subcriber.component";
import SubcriberList from
	"./Components/subcriber_list.component";

// App Component
const App = () => {
return (
	<Router>
	<div className="App">
		<header className="App-header">
		<Navbar className="color-nav"  variant="dark">
			<Container>
      <img
        src={logo}
        width="60"
        height="60"
      />
			<Navbar.Brand>
				<Link to={"/create-subcriber"}
				className="nav-link">
				ConsoInvest Formulaire Adhésion des Membres
				</Link>
			</Navbar.Brand>

			<Nav className="justify-content-end">
				<Nav>
				<Link to={"/create-subcriber"}
					className="nav-link">
					Nouveau Membre
				</Link>
				</Nav>

				<Nav>
				<Link to={"/subcriber-list"}
					className="nav-link">
					Listes des Membres
				</Link>
				</Nav>
			</Nav>
			</Container>
		</Navbar>
		</header>

		<Container>
		<Row>
			<Col md={12}>
			<div className="wrapper">
				<Routes>
				<Route exact path="/"
					  element={<CreateSubcriber/>} />
				<Route path="/create-subcriber"
					element={<CreateSubcriber/>} />
				<Route path="/edit-subcriber/:id"
					element={<EditSubcriber/>} />
				<Route path="/subcriber-list"
					element={<SubcriberList/>} />
				</Routes>
			</div>
			</Col>
		</Row>
		</Container>
	</div>
	</Router>
);
};

export default App;
