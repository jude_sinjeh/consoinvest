module.exports = (sequelize, Sequelize) => {
    const Subcriber = sequelize.define("tutorial", {
      name: {
        type: Sequelize.STRING
      },
      email: {
        type: Sequelize.STRING
      },
      rollno: {
        type: Sequelize.BOOLEAN
      }
    });
  
    return Subcriber;
};
